provider "aws" {
  version = "2.61"
}

data "aws_route53_zone" "primary" {
  name         = "gitops-demo.com."
  private_zone = false
}

resource "aws_route53_record" "drift" {
  zone_id = data.aws_route53_zone.primary.zone_id
  name    = "drift"
  type    = "A"
  ttl     = "300"
  records = ["8.8.8.8"]
}
